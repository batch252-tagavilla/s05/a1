-- 01.
SELECT customerName FROM customers 
WHERE country = "Philippines";

-- 02.
SELECT contactLastName, contactFirstName FROM customers 
WHERE customerName = "La Rochelle Gifts";

-- 03.
SELECT productName, MSRP FROM products 
WHERE productName = "The Titanic";

-- 04.
SELECT firstName, lastName FROM employees 
WHERE email = "jfirrelli@classicmodelcars.com";

-- 05.
SELECT customerName FROM customers 
WHERE state IS NULL;

-- 06.
SELECT firstName, lastName, email FROM employees 
WHERE lastName = "Patterson" AND firstName = "Steve";

-- 07.
SELECT customerName, country, creditLimit FROM customers
WHERE country != "USA" AND creditLimit > 3000;

-- 08.
SELECT customerNumber FROM orders
WHERE comments LIKE "%DHL%";

-- 09.
SELECT * FROM productlines
WHERE textDescription LIKE "%state of the art%";

-- 10.
SELECT DISTINCT country FROM customers;

-- 11.
SELECT DISTINCT status FROM orders;

-- 12.
SELECT customerName, country FROM customers
WHERE country IN ("USA", "France", "Canada");

-- 13.
SELECT employees.firstName, employees.lastName, offices.city FROM employees
JOIN offices ON employees.officeCode = offices.officeCode
WHERE offices.city = "Tokyo";

-- 14.
SELECT customers.customerName FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE employees.firstName = "Leslie" and employees.lastName = "Thompson";

-- 15.
SELECT products.productName, customers.customerName FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber
WHERE customers.customerName = "Baane Mini Imports";

-- 16.
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM orderdetails
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
JOIN offices ON employees.officeCode = offices.officeCode
WHERE customers.country = offices.country;

-- 17.
SELECT productName, quantityInStock FROM products
WHERE productLine = "planes" AND quantityInStock < 1000;

-- 18.
SELECT customerName FROM customers
WHERE phone LIKE "%+81%";